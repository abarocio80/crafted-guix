(define-module (crafted home services crafted-emacs)
  ;; Crafted modules
  #:use-module (crafted packages emacs)
  ;; GNU modules
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services utils)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages version-control)
  ;; Guix modules
  #:use-module (guix gexp)
  #:use-module (guix packages)
  
  ;; Our exports
  #:export (home-crafted-emacs-service-type
	    home-crafted-emacs-configuration))

;; stolen from gnu/services/cgit.scm
(define (uglify-field-name field-name)
  (string-delete #\? (symbol->string field-name)))

(define (serialize-field field-name val)
  #~(format #f "~a=~a\n" #$(uglify-field-name field-name) #$val))

(define (serialize-string field-name val)
  (if (and (string? val) (string=? val ""))
      ""
      (serialize-field field-name val)))


(define-configuration home-crafted-emacs-configuration
  (directory
   (string ".emacs.d")
   "The Crafted Emacs directory.")
  (package
   (package crafted-emacs)
   "The Crafted Emacs package to use.")
  (emacs-package
   (package emacs)
   "The Emacs package to use.")
  (config-file
   (text-config '())
   "the config.el file.")
  (early-config-file
   (text-config '())
   "The early-config.el file."))

(define package-user-dir "~/.cache/emacs/elpa")

(define (home-crafted-emacs-files-service config)
  (list `(,(home-crafted-emacs-configuration-directory config) ,crafted-emacs)
	`(".config/crafted-emacs/config.el"
	  ,(mixed-text-file "config.el"
			    ";;; config.el --- Crafted Emacs Config from Guix Home.\n\n"
			    (home-crafted-emacs-configuration-config-file config)))
	`(".config/crafted-emacs/early-config.el"
	  ,(mixed-text-file "early-config.el"
			    ";;; early-config.el --- Crafted Emacs Early Config from Guix Home.\n\n"
			    (format #f "(make-directory ~s t)\n" package-user-dir)
			    (format #f "(setq package-user-dir ~s)\n" package-user-dir)
			    (home-crafted-emacs-configuration-early-config-file config)))))

(define (home-crafted-emacs-profile-service config)
  (list nss-certs
	git
	(home-crafted-emacs-configuration-package config)
	(home-crafted-emacs-configuration-emacs-package config)))

(define home-crafted-emacs-service-type
  (service-type (name 'home-crafted-emacs)
		(description "The home service to configure Crafted Emacs.")
		(extensions
		 (list (service-extension
			home-files-service-type
			home-crafted-emacs-files-service)
		       (service-extension
			home-profile-service-type
			home-crafted-emacs-profile-service)))
		(default-value (home-crafted-emacs-configuration))))

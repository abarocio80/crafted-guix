(define-module (crafted home-config)
  ;; The Crafted modules
  #:use-module (crafted home services crafted-emacs)
  ;; The GNU modules
  #:use-module (gnu)
  #:use-module (gnu services)
  #:use-module (gnu home)
  #:use-module (gnu home services)
)

(home-environment
 (services
  (list
   (service home-crafted-emacs-service-type
	    (home-crafted-emacs-configuration
	     (directory ".config/emacs")
	     (config-file
	      (string-append
	       "(load-theme 'modus-vivendi)\n"
	       "(disable-theme 'deeper-blue)\n"
	       ;; "(load-library \"crafted-ui\")\n"
	       "(load-library \"crafted-editing\")\n"
	       ;; "(load-library \"crafted-evil\")"
	       ))
	     (early-config-file ""))))))

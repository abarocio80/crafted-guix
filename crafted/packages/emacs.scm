(define-module (crafted packages emacs)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define-public crafted-emacs
  (let ((url-homepage "https://github.com/SystemCrafters/crafted-emacs/"))
  (package
    (name "crafted-emacs")
    (version "0.1")
    (source (origin
	      (method git-fetch)
	      (uri (git-reference
		    (url url-homepage)
		    (commit "2fa5bf762245bfe8063ec83cefdaf09b7a3b4d8e")))
	      (sha256
	       (base32
		"18kh853jhni2k3q7scsx3hhgxjpnggsrrdmfi15jhfhdi9f3a5f0"))))
    (build-system copy-build-system)
    (home-page url-homepage)
    (synopsis "A sensible starting point for hacking your own Emacs configuration.")
    (description "A sensible starting point for hacking your own Emacs configuration.

Please note: crafted-emacs is under active development. While the
intent of this project is to provide a stable Emacs configuration for
use by anyone, it is not yet stable enough for everyday or production
use. The rapid pace of changes could cause your configuration to break
on your next pull of the repository. If you are not expecting or
prepared to encounter such issues, we would recommend you wait for
things to stabilize a bit before using.

* Principles

This configuration and all associated modules intend to follow the below priniciples.

** Minimal, modular configuration ::
The core configuration only sets up Emacs to have a cleaner
presentation with sensible defaults. It is up to the user to decide
which of the various crafted-* modules to load and when to load them.

Configuration modules should depend on other modules and the base
configuration as little as possible. When a configuration module needs
to integrate with other functionality in Emacs, the standard
extensibility points of each package should be used (instead of
expecting our own configuration module).

The implication is that someone should be able to install or copy code
from a crafted-* module into their own configuration without using
Crafted Emacs.

** Prioritize built-in Emacs functionality ::
Where possible, we will leverage built-in Emacs functionality instead of external packages, for example:

 + project.el instead of Projectile

 + tab-bar-mode instead of Perspective.el, persp-mode, eyebrowse, etc

 + eglot instead of lsp-mode (because eglot prioritizes built-in functionality)

 + Possibly vc-mode by default

** Sensible folder layout ::
While Emacs tends to keep everything (code, configuration, state
files, …) inside `user-emacs-directory` modern computer systems tend
to keep those separated.

Crafted Emacs tries to maintain some balance between those two
paradigms by bringing just the right amount of order to it.

** Works well in the terminal ::
Some people prefer to use Emacs in the terminal instead of as a
graphical program. This configuration should work well in this case
too! This also enables the use of Emacs in Termux on Android.

** Can be integrated with a Guix configuration ::
It should be possible to customize aspects of the Crafted Emacs
configuration inside of a Guix Home configuration so that things like
font sizes, themes, etc can be system-specific.

It can also use packages installed via the Guix package manager
instead of package.el.

** Works well with Chemacs2 ::
Chemacs2 is an excellent tool for enabling the use of multiple Emacs
configurations simultaneously. This configuration will behave well
when used with Chemacs2 so that users can try and use different Emacs
configurations as needed.

** Helps you learn Emacs Lisp ::
Instead of providing a higher-level configuration system out of the
box like other Emacs configurations, we follow standard Emacs Lisp
patterns so that you can learn by reading the configuration.

** Reversible ::
Not everyone will agree with our decisions, so each customization
should be easily reversible in the users config.el file.")
    (license license:expat))))
